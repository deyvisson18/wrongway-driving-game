﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Spawn : MonoBehaviour
{
   public Button[] buttons;
   public Text scoreText;
   int score;
   bool gameOver;




    // Start is called before the first frame update
    void Start()
    {
        gameOver = false;
        score = 0;
        InvokeRepeating ("scoreUpdate", 1.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text= "Score: " + score;
        
    }

     void scoreUpdate(){
         if (gameOver==false)
         {
             score += 1;
         }
        
    }

    public void gameOverActivaded(){
        gameOver = true;
        foreach (Button button in buttons)
        {
            
            
            button.gameObject.SetActive(true);
        }

    }

    public void Play(){
        Application.LoadLevel ("Level1Scene");

    }

    public void Pause(){

            if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
            }
            else if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
    }

    public void Menu(){
        Application.LoadLevel ("MenuScene");
    }

    public void Exit(){
        Application.Quit();
    }
}
