﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawnPosition : MonoBehaviour
{
    public GameObject[] cars;
    int carNo;
    public float maxPos = 2.55f;
    public float delayTimer = 0.5f;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        timer = delayTimer;
    }

    // Update is called once per frame
    void Update()
    {

        timer -= Time.deltaTime;
        if (timer<=0)
        {
        Vector3 carPos = new Vector3(Random.Range(-2.55f, 2.55f), transform.position.y, transform.rotation.z);
        carNo= Random.Range(0,6);
        Instantiate (cars[carNo], carPos, transform.rotation);
        timer = delayTimer;
        }
        
        
    }
}
